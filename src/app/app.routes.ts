import { RouterModule, Routes } from '@angular/router';
import {ToolsComponent} from "./components/tools/tools.component";
import {ToolsEditComponent} from "./components/tools-edit/tools-edit.component";

const app_routes: Routes = [
  { path: 'tools', component: ToolsComponent },
    { path: 'tools-edit/:id', component: ToolsEditComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'tools' }
];

export const APP_ROUTING = RouterModule.forRoot(app_routes);
