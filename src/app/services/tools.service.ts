import { Injectable } from '@angular/core';
import { Http , Headers} from '@angular/http';
import { ToolsEdit } from "../interfaces/tools.interfaces";
import 'rxjs/Rx';

@Injectable()
export class ToolsService {
fireUrl:string ="https://securityapp-f46eb.firebaseio.com/tools.json";
Url:string="https://securityapp-f46eb.firebaseio.com/tools/";
  constructor( private http:Http) { }
nuevatools(tools:ToolsEdit){

let body = JSON.stringify(tools);
let headers = new Headers({
  'content-Type':'application/json'
});

return this.http.post(this.fireUrl,body,{headers})
 .map(res=>
{
  console.log(res.json());
  return res.json();
})
  }
  actulizartools(tools:ToolsEdit,key$:string){

  let body = JSON.stringify(tools);
  let headers = new Headers({
    'content-Type':'application/json'
  });
  let url ='${this.Url}/${key$}.json';
  return this.http.put(url,body,{headers})
   .map(res=>{
    console.log(res.json());
    return res.json();
  })
    }
getTools(key$:string){
  let url ='${this.Url}/${key$}.json';
  return this.http.get(url)
  .map(res=>res.json());
}
getToolsE(){

  return this.http.get(this.fireUrl)
  .map(res=>res.json());
}

}
