import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router,ActivatedRoute } from "@angular/router";
import { ToolsEdit } from "../../interfaces/tools.interfaces";
import { ToolsService } from "../../services/tools.service";

@Component({
  selector: 'app-tools-edit',
  templateUrl: './tools-edit.component.html',
  styles: []
})
export class ToolsEditComponent implements OnInit {
tools:ToolsEdit = {
nombre:"",
bio:""
}
nuevo:boolean =false;
id:string;
  constructor(private _toolsService: ToolsService,
               private router:Router,
                private route :ActivatedRoute  ) {

      this.route.params
    .subscribe(parametros=>{
      this.id = parametros['id']
      if(this.id !="nuevo"){
        this._toolsService.getTools(this.id)
        .subscribe(tools =>this.tools=tools);
      }
    });
                }

  ngOnInit() {
  }
  guardar(){
    console.log(this.tools);
    if(this.id =="nuevo"){
      this._toolsService.nuevatools(this.tools)
          .subscribe(data => {
                this.router.navigate(['/tools-edit',data.name])
          },
          error =>console.log(error));
    }else{
      this._toolsService.actulizartools(this.tools,this.id)
          .subscribe(data => {
              console.log(data);
          },
          error =>console.log(error));
    }


   }
   agregarnuevo(forma:NgForm){
     this.router.navigate(['/tools-edit','nuevo']);
     forma.reset();
   }
}
