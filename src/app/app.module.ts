import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from  "@angular/forms";
import {APP_ROUTING} from "./app.routes";
import { HttpModule } from '@angular/http';

import { ToolsService } from './services/tools.service';
import { AppComponent } from './app.component';
import { ToolsComponent } from './components/tools/tools.component';
import { ToolsEditComponent } from './components/tools-edit/tools-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolsComponent,
    ToolsEditComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    FormsModule,
     HttpModule
  ],
  providers: [ToolsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
